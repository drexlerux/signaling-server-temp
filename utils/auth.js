const request = require('request')
const config = require('../config')

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

module.exports = async function(req, username, password, done){ 
    log(chalk.cyan(config.authEndpoint()))
    request.post({url: config.authEndpoint(), form: {email: username, token: password}}, function(e, r, body){
        if(IsJsonString(r.body)){
            var response = JSON.parse(r.body);
            if(response.success){
                response.user.role = "interviewer";
                return done(null, response.user)
            }else{
                return done(null, false, req.flash('error', response.msg))
            }     
        }else{
            return done(null, false, req.flash('error', r.body))
        }

    });
	
}