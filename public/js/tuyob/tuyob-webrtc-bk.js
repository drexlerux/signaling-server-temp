function TuyobWebRTC(settings){
    // ......................................................
    // ..................RTCMultiConnection Code.............
    // ......................................................
    this.settings = settings;
    this.connection = new RTCMultiConnection();

    // by default, socket.io server is assumed to be deployed on your own URL
    this.connection.socketURL = settings.serverUrl;

    // comment-out below line if you do not have your own socket.io server
    // connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

    this.connection.socketMessageEvent = 'audio-video-file-chat-demo';

    this.connection.enableFileSharing = true; // by default, it is "false".

    this.connection.session = {
        audio: {
            echoCancellation: true,
            autoGainControl: true,
            noiseSuppression: true,
            volume: 0.8,
        },
        video:{
            width: { ideal: 1280 },
            height: { ideal: 1024 },
            facingMode: "environment"
        },
        data: true
    };

    this.connection.sdpConstraints.mandatory = {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: true
    };

    this.connection.filesContainer = document.getElementById('file-container');


    this.videoLocal = document.getElementById('local');
    this.videoRemote  = document.getElementById('remote')
    connect(this);
    bindEvents(this);
}

TuyobWebRTC.prototype.on = function(methodName, cb){
    TuyobWebRTC.prototype[methodName] = cb
}

TuyobWebRTC.prototype.trigger = function(mn, args){
    args = args || [];
    if(typeof this[mn] === "function"){
        TuyobWebRTC.prototype[mn].call(this, ...args)
    }
}

TuyobWebRTC.prototype.sendChatMessage = function(data){
    var ctx = this;
    data.room = this.settings.room;
    data.type = 'text';
    saveToServer(window.location.origin+"/saveChat", data, function(rdata){
        ctx.connection.send(rdata);
        ctx.trigger('self-chat-message', [rdata]);
       
    });
    
}


function bindEvents(ctx){
    ctx.connection.onstream = function(event) {
  
        if(event.type === 'local') {
            renderVideo(ctx.videoLocal, event)
        }else{
            renderVideo(ctx.videoRemote, event)
        }
        
    };

    ctx.connection.onstreamended = function(event) {
        var mediaElement = document.getElementById(event.streamid);
        if (mediaElement) {
            mediaElement.parentNode.removeChild(mediaElement);
        }
    };

    ctx.connection.onmessage = function(data){
        console.log("Hola: ", data);
        ctx.trigger('chat-message', [data])
    };

    ctx.connection.onopen = function() {

    };

    ctx.connection.onclose = function() {
        if (ctx.connection.getAllParticipants().length) {
            
        } else {
           
        }

        var videoRemote = document.querySelector('#video-remote video')
        videoRemote.style.display = "none";
    };

    ctx.connection.onEntireSessionClosed = function(event) {
        ctx.connection.attachStreams.forEach(function(stream) {
            stream.stop();
        });

        // don't display alert for moderator
        //if (ctx.connection.userid === event.userid) return;
        //document.querySelector('h1').innerHTML = 'Entire session has been closed by the moderator: ' + event.userid;
    };

    ctx.connection.onUserIdAlreadyTaken = function(useridAlreadyTaken, yourNewUserId) {
        // seems room is already opened
        ctx.connection.join(useridAlreadyTaken);
    };
}

function renderVideo(video, event){
    video.srcObject = event.stream;
}


function connect(ctx){
      var ROLES = {
          INTERVIEWER: 'interviewer',
          INTERVIEWED: 'interviewed'
      }

      switch(ctx.settings.role){
        case ROLES.INTERVIEWER:
            ctx.connection.open(ctx.settings.room);
            break;

        case ROLES.INTERVIEWED:
            ctx.connection.join(ctx.settings.room);
            break;
        default:
            console.log("invalid role");
    }
}


function saveToServer(url, data, callback){

        var xmlhttp;
        // compatible with IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                callback(JSON.parse(xmlhttp.responseText));
            }
        }

        xmlhttp.open("POST", url, true);
        xmlhttp.setRequestHeader("Content-Type", "application/json");

        //xmlhttp.setRequestHeader(header.name, header.value);
        
        xmlhttp.send(JSON.stringify(data));
}

window.TuyobWebRTC = TuyobWebRTC;