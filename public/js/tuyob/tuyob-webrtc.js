const EVENTS ={
    HANGUP: 'hang-up',
    CHECK_IN: 'checkIn',
    CHECK_OUT: 'checkOut'
}

const STATUS ={
    STARTED: 'started',
}

function TuyobWebRTC(settings){
    // ......................................................
    // ..................RTCMultiConnection Code.............
    // ......................................................
    this.settings = settings;
    this.connection = new RTCMultiConnection();

    // by default, socket.io server is assumed to be deployed on your own URL
    this.connection.socketURL = (typeof settings.socketURL !== 'undefined') ? settings.socketURL : "/";

    this.connection.apiUrl = (typeof settings.apiUrl !== 'undefined') ? settings.apiUrl : window.location.origin;

    // comment-out below line if you do not have your own socket.io server
    // connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

    this.connection.socketMessageEvent = 'audio-video-file-chat-demo';

    this.connection.enableFileSharing = true; // by default, it is "false".

    this.connection.session = {
        audio: {
            mandatory: {
                echoCancellation: true, // disabling audio processing
                googAutoGainControl: true,
                googNoiseSuppression: true,
                googHighpassFilter: true,
                googTypingNoiseDetection: true,
                volume: 0.8,
            },
        },
        video: {
            mandatory: {
                maxWidth: 1920,
                maxHeight: 1080
            }
        },
        data: true
    };

    this.connection.bandwidth = { 
        audio: 50, 
        video: 256 
    }; 

    this.connection.sdpConstraints.mandatory = {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: true
    };

    this.connection.filesContainer = document.getElementById('file-container');


    this.videoLocal = document.getElementById('local');
    this.videoRemote  = document.getElementById('remote')


    this.ROLES = {
        INTERVIEWER: 'interviewer',
        INTERVIEWED: 'interviewed'
    }

    this.socket = this.connection.getSocket();

    this.reloadPage = true;

    connect(this);
    bindEvents(this);
}

TuyobWebRTC.prototype.on = function(methodName, cb){
    TuyobWebRTC.prototype[methodName] = cb
}

TuyobWebRTC.prototype.trigger = function(mn, args){
    args = args || [];
    if(typeof this[mn] === "function"){
        TuyobWebRTC.prototype[mn].call(this, ...args)
    }
}

TuyobWebRTC.prototype.sendChatMessage = function(data, opts){
    opts = opts || {};
    var options = merge(opts ,{ 
        headers: [], 
        endpoint: '/saveMessage'
    });
    var ctx = this;
    data.category = 'PLAIN-TEXT';
    data.room = ctx.settings.room;
    saveToServer(this.connection.apiUrl + options.endpoint, data, function(rdata){
        ctx.trigger('self-chat-message', [rdata]);
        ctx.connection.send(rdata);
    });
    
}

TuyobWebRTC.prototype.warnLeave = function(){
    this.connection.socket.emit('warn-leave');
}

TuyobWebRTC.prototype.loadMessages = function(opts, callback){
    var options = merge(opts ,{ 
        headers: [],
        endpoint: '/chats/'+this.settings.room
    });

    var xmlhttp;
    // compatible with IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
            callback(JSON.parse(xmlhttp.responseText));
        }
    }
    xmlhttp.open("GET", this.connection.apiUrl + options.endpoint, true);
    if(typeof options.headers !== 'undefined'){
        options.headers.forEach(function(header){
            xmlhttp.setRequestHeader(header.name, header.value);
        });
        xmlhttp.send();
    }else{
        console.error("Error to load mesagges, the secret token does not exist")
    }

}


TuyobWebRTC.prototype.hangUp = function(){
    this.reloadPage = false;
    this.connection.send({eventType: EVENTS.HANGUP})    
}


function bindEvents(ctx){
    ctx.connection.onstream = function(event) {
  
        if(event.type === 'local') {
            renderVideo(ctx.videoLocal, event)
        }else{
            renderVideo(ctx.videoRemote, event)
        }
        
    };

    ctx.connection.onstreamended = function(event) {
        if (ctx.videoLocal) {
            //ctx.videoLocal.parentNode.removeChild(ctx.videoLocal);
        }
    };

    ctx.connection.onmessage = function(data){
        if(typeof data.data.eventType !== 'undefined'){
            switch(data.data.eventType){
                case EVENTS.HANGUP:
                    ctx.reloadPage = false;
                    ctx.trigger(EVENTS.HANGUP)
                break;
            }
        }else{
            ctx.trigger('chat-message', [data])
        }
        
    };

    ctx.connection.onopen = function(e) {
        console.warn(e);
    };

    ctx.connection.onclose = function() {
        ctx.videoRemote.style.display = "none";

        const noftData = {
            role: ctx.settings.role,
            room: ctx.settings.room, 
            event: EVENTS.CHECK_OUT,
            status: STATUS.END
        }

        if(ctx.reloadPage){
            toastr.error('Se ha desconectado de la videollamada', 'UPSS', {
                positionClass: "toast-top-left"
            })            
        }

        //notifyToServer(ctx, noftData);


    };

    ctx.connection.onEntireSessionClosed = function(event) {
        ctx.connection.attachStreams.forEach(function(stream) {
            stream.stop();
        });

        // don't display alert for moderator
        //if (ctx.connection.userid === event.userid) return;
        //document.querySelector('h1').innerHTML = 'Entire session has been closed by the moderator: ' + event.userid;
    };

    ctx.connection.onUserIdAlreadyTaken = function(useridAlreadyTaken, yourNewUserId) {
        // seems room is already opened
        ctx.connection.join(useridAlreadyTaken);
    };    

    ctx.connection.onleave = function(e) {

    };

    window.onbeforeunload = function(){
        const noftData = {
            role: ctx.settings.role,
            room: ctx.settings.room, 
            event: EVENTS.CHECK_OUT,
            status: STATUS.END
        }

        notifyToServer(ctx, noftData)
    }

}

function renderVideo(video, event){
    video.srcObject = event.stream;
}


function connect(ctx){
    const noftData = {
        room: ctx.settings.room, 
        event: EVENTS.CHECK_IN,
        status: STATUS.STARTED
    }

    switch(ctx.settings.role){
        case ctx.ROLES.INTERVIEWER:
            noftData.role = ctx.ROLES.INTERVIEWER;
            ctx.connection.open(ctx.settings.room, function(){
                notifyToServer(ctx, noftData);
            });
            break;

        case ctx.ROLES.INTERVIEWED:
            noftData.role = ctx.ROLES.INTERVIEWED;
            ctx.connection.join(ctx.settings.room, function(){
                notifyToServer(ctx, noftData);
            });

            break;
        default:
            console.log("invalid role");
    }

}


function saveToServer(url, data, callback){

        var xmlhttp;
        // compatible with IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                callback(JSON.parse(xmlhttp.responseText));
            }
        }

        xmlhttp.open("POST", url, true);
        xmlhttp.setRequestHeader("Content-Type", "application/json");

        //xmlhttp.setRequestHeader(header.name, header.value);
        
        xmlhttp.send(JSON.stringify(data));
}

function notifyToServer(ctx, data){
        var xmlhttp;
        // compatible with IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                console.log(JSON.parse(xmlhttp.responseText));
            }
        }

        xmlhttp.open("POST", window.location.origin + "/" + data.event, true);
        xmlhttp.setRequestHeader("Content-Type", "application/json");

        //xmlhttp.setRequestHeader(header.name, header.value);
        
        xmlhttp.send(JSON.stringify(data));
}

window.TuyobWebRTC = TuyobWebRTC;