(function($){
  $.fn.focusTextToEnd = function(){
      this.focus();
      var $thisVal = this.val();
      this.val('').val($thisVal);
      return this;
  }
}(jQuery));

$('button.clipboard').tooltip({
  trigger: 'click',
  placement: 'bottom'
});

function setTooltip(message, target) {
  $(target).tooltip('hide')
    .attr('data-original-title', message)
    .tooltip('show');
}

function hideTooltip(target) {
  setTimeout(function() {
    $(target).tooltip('hide');
  }, 1000);
}

var clipboard = new ClipboardJS('button.clipboard',{
    text: function(trigger) {
        return window.location.origin + trigger.getAttribute('data-clipboard-text');
    }
});

clipboard.on('success', function(e) {
  	setTooltip('Copiado!', e.trigger);
  	hideTooltip(e.trigger);
});

clipboard.on('error', function(e){
	setTooltip('Fallo!', e.trigger);
		hideTooltip(e.trigger);
});

$('.edit-interview').on('click', function(){
  $self = $(this);

  $('tbody tr').each(function(){
    console.log('hola');
    $(this).children('td').find('.edit-interview').prop('disabled', false)
    $(this).children('.name-interviewed').find('i.fa-times').trigger('click');
  });
 

  $self.prop('disabled', true);
  var interviewedTd = $(this).parent().siblings('.name-interviewed');
  var interviewedName = interviewedTd.text();
  $dom = $("<div><input type=\"text\" name= \"interviewed\"value=\""+interviewedName+"\" autocomplete=\"off\"> <i class=\"fa fa-check\"></i> <i class=\"fa fa-times\"></i></div>");
  interviewedTd.html($dom);
  
  
  $dom.find('i.fa-check').on('click', function(){
    var valueInput = $dom.find('input').val().trim();
    if(valueInput !== ''){
      $.ajax({
        url: '/interview/edit/'+$self.data('id'),
        method: 'put',
        dataType: 'json',
        data: {interviewed: $dom.find('input').val()},
        success: function(response){
          if(response.success){
            interviewedTd.text(valueInput);
            $self.prop('disabled', false);
          }
        }
      });
    }
  })

  $dom.find('i.fa-times').on('click', function(){
    interviewedTd.text(interviewedName);
    $self.prop('disabled', false);
  })
  $dom.find('input').focusTextToEnd();
});