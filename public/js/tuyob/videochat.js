var colors = {
    interviewer:  "1b60cf",
    interviewed: "1b988a"
}

var linkAvatar = "https://ui-avatars.com/api/?rounded=true&background=<background-color>&color=fff&name=<user-name>";

function merge(obj, extended){
    for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            // Push each value from `obj` into `extended`
            extended[prop] = obj[prop];
        }
    }

    return extended;
}

function evaluate(vue, item) {
    var message = {
        content: item.content
    };

    message = merge(item, message)

    if(params.role == item.role){
        message.isSelf = true;
        message.avatar = vue.user.local.avatar;
    }else{
        message.isSelf = false;
        message.avatar = vue.user.remote.avatar;
    }
    return message;
}


function onResizeWindow(vue){
    var iWidth = $( window ).width();
    if(iWidth <= 1000){
        vue.showChat = false;
    }
    $(window).resize(function(){
        var windowWidth = $( window ).width();
        if($( window ).width() !== iWidth && windowWidth <= 1000){
            vue.showChat = false;
        }else{
            vue.showChat = true;
        }
        iWidth = windowWidth;
    });
}


function updateScroll(time, callback) {
    callback = callback ||  null;
    $('.messages-container')
        .animate({
        scrollTop: $(".chat-item").parent().height()
    }, time, 'swing', callback);
}

function onEvents(vue){
    vue.vc.on('chat-message', function(chatData){
        console.error(chatData.data)
        vue.messages.push(evaluate(vue, chatData.data));
        updateScroll(3000);
    });

    vue.vc.on('self-chat-message', function(chatData){
        console.error(chatData)
        vue.messages.push(evaluate(vue, chatData));
        updateScroll(3000);
    });

    vue.vc.on('hang-up', function () {
        showCloseInterview(vue);
    });
}


function loadInitMessages(vue){
    $wait_c = $('<div class="wait-container"><div>');
    $('.chat-body').append($wait_c);
    vue.vc.loadMessages({
        headers: [{name: 'secret', value:'aWlLkr9MwLRYovT1EKWu'}]
    }, function(messages) {
        console.log(messages);
        messages.forEach(function(item, index){
            messages[index] = evaluate(vue, item);
        });
        vue.messages = messages;
        setTimeout(function () {
            updateScroll(0, function () {
                $wait_c.fadeOut();
            });
        })

    });
}

function assignProfiles(vue){
    if(params.role == vue.vc.ROLES.INTERVIEWER){
        vue.user.local.name = params.interviewerName;
        vue.user.local.avatar = linkAvatar.replace("<background-color>", colors[vue.vc.ROLES.INTERVIEWER]).replace("<user-name>", params.interviewerName)

        vue.user.remote.name =  params.interviewedName;
        vue.user.remote.avatar = linkAvatar.replace("<background-color>", colors[vue.vc.ROLES.INTERVIEWED]).replace("<user-name>", params.interviewedName)
    }else{

        vue.user.local.name =  params.interviewedName;
        vue.user.local.avatar = linkAvatar.replace("<background-color>", colors[vue.vc.ROLES.INTERVIEWED]).replace("<user-name>", params.interviewedName)

        vue.user.remote.name =  params.interviewerName;
        vue.user.remote.avatar = linkAvatar.replace("<background-color>", colors[vue.vc.ROLES.INTERVIEWER]).replace("<user-name>",  params.interviewerName)
    }
}


function  showCloseInterview(vue) {
    //vue.vidCh.stopRecording();
    vue.recording = false;
    
    swal({
        title: 'Entrevista finalizada',
        text: 'Gracias por su colaboracion en este proceso, pronto le estaremos contactando',
        type: 'success'
    }).then((result) => {
        if (result.value) {
            location.href = 'https://tuyob.com.co';
        }
    })
}

new Vue({
  el: "#app",
  data: function(){
    return {
        vc: null,
        user:{
            local:{
                name: "",
                avatar: ""
            },
            remote: {
                name: "",
                avatar: ""
            },
            role: params.role
        },

        messageText: '',
        messages: [

        ],

        showChat: true,


        recording: false,
        minutes: '30',
        seconds: '00',

        awaitingToHangup: false
    }
  },

  mounted: function(){

    this.vc = new TuyobWebRTC({
        role: params.role,
        room: params.room
    })

    if (window.matchMedia("(max-width: 550px)").matches) this.showChat = false;

    assignProfiles(this);
    loadInitMessages(this);
    onEvents(this);
        
  },

  methods:{
    sendMessage: function(){
        if(this.messageText.trim().length > 0){
            this.vc.sendChatMessage({
                content: this.messageText.trim(),
                username: this.user.local.name,
                role: params.role,
                avatar: this.user.local.avatar
            });
            this.messageText = '';

        }
    },

    toggleChatContainer: function(){
        if(this.showChat){
            this.showChat = false;
        }else{
            this.showChat = true;
        }
    },

    warnLeave: function () {
        var ctx = this;
        ctx.awaitingToHangup = true;
        swal({
            'title': '¡Desea cerrar la entrevista?',
            'type': 'question',
            'confirmButtonText': 'Sí',
            'showCancelButton': true,
            'cancelButtonColor': '#D50000',
            'cancelButtonText': 'No',
        }).then(function (result) {
            if(result.value){
                ctx.vc.hangUp();
                $('.call-btns').html('<button class="btn btn-primary bw"><i class="fa fa-clock-o fa-2x"></i></button>')
                showCloseInterview(ctx);
            }else{
                ctx.awaitingToHangup = false;
            }
        });
    },

  }
});