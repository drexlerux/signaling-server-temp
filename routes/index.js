const express = require('express')
const router = express.Router()
const moment = require('moment-timezone');
const ChatModel = require('../models/chat')
const ChatSchema = require('../schemas/chat')
const InterviewSchema = require('../schemas/interview')
var passport = require('passport');
const config = require('../config');
const request = require('request');
const authentication = require('../middlewares/auth');
const rdmRoom = require('../utils/helpers').rdmAlphaNumericStr

const connectedUsers = {};

router.get('/one2one/:room/interviewer', authentication, function(req, res, next) {
	InterviewSchema.findOne({room: req.params.room}, function(err, interview){
		if(err || !interview)
			res.status(404).send('404 page');
		else
  		res.render('one2one', {room: req.params.room, role:'interviewer', interviewerName: interview.interviewer, interviewedName: interview.interviewed});
	})
})

router.get('/one2one/:room/interviewed', function(req, res, next) {
	InterviewSchema.findOne({room: req.params.room}, function(err, interview){
		InterviewSchema.findOne({room: req.params.room}, function(err, interview){
			if(err || !interview)
				res.status(404).send('404 page');
			else
				res.render('one2one', {room: req.params.room, role:'interviewed', interviewerName: interview.interviewer, interviewedName: interview.interviewed});
		})
  		
	})
})


router.post('/logout', function(req, res, next){
	req.logout()
	res.redirect('/')
})

router.post('/saveMessage', function(req, res, next) {
	ChatModel.save(req.body, function(result){
		res.send(result)
	})
});


router.get("/", function(req, res, next){
	res.render('index');
});





/*router.post("/", passport.authenticate('local', {
	failureRedirect: '/',
	failureFlash: true,
	//successRedirect: '/mierda',
	badRequestMessage: 'Datos incompletos',
}), function(req, res) {
	res.redirect(301, "/begin");
});*/


router.post('/', passport.authenticate('local', {
  successRedirect: '/start',
  failureRedirect: '/',
	failureFlash: true,
}));

router.get("/start/:page?",  authentication, function(req, res, next){
	const perPage = 10;
	const page = parseInt(req.params.page) || 1;

	InterviewSchema.find({idInterviewer: req.user.id})
		.skip((page-1)*perPage)
		.sort({date: -1})
		.limit(perPage)
		.exec(function(err, interviews){
			InterviewSchema.find({idInterviewer: req.user.id}).count().exec(function(err2, count){
				if(err || err2){
					req.flash('error', 'Error al cargar los registros')
					res.render('start', {user: req.user, interviews: [], pages: 0, nItems: 0})
				}else{
					res.render('start', {user: req.user, interviews, pages: Math.ceil(count/perPage), nItems: count, page, perPage})
				}
			})
		}
	)
});

router.post('/interview/add', function(req, res, next){
	var data = {
		interviewed: req.body.interviewed,
		idInterviewer: req.user.id,
		interviewer: req.user.name, room: rdmRoom()
	}

	InterviewSchema.create(data, function(err, result){
		if(err){
			req.flash('error', 'Error al crear la entrevista')
			log(chalk.green(JSON.stringify(err)))
		}
		
		res.redirect(req.get('referer'));
	});

})

router.put('/interview/edit/:id', function(req, res, next){
	InterviewSchema.update({_id: req.params.id}, req.body, function(err, r){
		if(err)
			res.send({success: false}).end()
		res.send({success: true}).end()
	})
})

router.post("/checkIn", function(req, res, next){
	if(typeof connectedUsers[req.body.room] === 'undefined')
		connectedUsers[req.body.room] = {};
	connectedUsers[req.body.room][req.body.role] = true;
	res.send(connectedUsers[req.body.room])	
});


router.post("/checkOut", function(req, res, next){
	delete connectedUsers[req.body.room][req.body.role];
	log(chalk.red("checkOut: " + JSON.stringify(connectedUsers)))
	next();
});

router.get('/chats/:room', function(req, res, next){
  
  if(config.secret == req.get('secret')){
    ChatSchema.find({room: req.params.room}, function(err, chats){
      res.type('json')
      if(err)
        res.status(504).end()
      else
        res.status(200).send(chats)
    })
  }else{
    res.type('json')
    res.status(401).send({msg: 'Unauthorized'})
  }
})

module.exports = router