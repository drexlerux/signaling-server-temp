const express = require('express')
const router = express.Router()
const moment = require('moment')
const Chat = require('../models/chat')
router.get('/conference/:room/:role', function(req, res, next) {
  res.render('index', req.params)
})


router.post('/saveChat', function(req, res, next) {
	Chat.save(req.body, function(result){
		var rdata = {
			content: result.content,
			role: result.role,
			type: result.type,
			username: result.username,
			date: moment(result.createdAt).format('YYYY-MM-DD'),
			time: moment(result.createdAt).format('hh:mm a'),
			avatar: req.body.avatar
		};

		res.send(req.body)
  		
	})
	
})

module.exports = router