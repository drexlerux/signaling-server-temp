const ChatSchema = require('../schemas/chat')

const Chat = function(){}

Chat.prototype.save = function(data, cb){
    const chat = new ChatSchema(data)
    chat.save(function(err, results){
        if(!err && results){    
            cb(results)
        }
    })
}

module.exports = new Chat()