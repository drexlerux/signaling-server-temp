const InterviewSchema = require('../schemas/interview')

const Interview = function(){}

Interview.prototype.set = function(data){
    InterviewSchema.findOne({room: data.room}, function(err, stInterview){ 
        if(!err){
            if(!stInterview){
                const interview = new InterviewSchema(data)
                interview.save() 
            }
        }
    })
}

Interview.prototype.setRecordTime = function(_room, _timerData){
    InterviewSchema.update(
        {room: _room},
        {$set: {recordTimingLeft: _timerData}},
        function(){}
    )
}

Interview.prototype.unsetPeer = function(_peer, _room){

    InterviewSchema.update(
        {room: _room},
        {
            $pull: {
                peers: {id: _peer}
            }
        },
        function(err, doc){}
    )
}

Interview.prototype.getTiming = function(room, cb){
    InterviewSchema.findOne({room}, function(error, doc){
        const ok = typeof doc.recordTimingLeft !== 'undefined' &&
                   typeof doc.recordTimingLeft.distance !== 'undefined'
        if(ok)
            cb(ok, doc.recordTimingLeft)
        else
            cb(ok)
    })
}

Interview.prototype.updateStatus = function(_room, _status){
    InterviewSchema.update(
        {room: _room},
        {$set: {status: _status}},
        function(error, result){
        }
    )
}

Interview.prototype.pushRole = function(_room, _role){
    InterviewSchema.findOne({'room': _room, roles: {$in: [_role]}}, function(err, doc){
        if(!doc){
            InterviewSchema.update({'room': _room}, {$push: {roles: _role}}, function(err1, result){
                console.log("Err:  ", result)
            })
        }
    })
}

Interview.prototype.getAll = function(_criteria, select, cb){
    InterviewSchema.find(_criteria, select, cb)
}

module.exports = new Interview()