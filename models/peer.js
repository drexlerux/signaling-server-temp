const PeerSchema = require('../schemas/peer')

const Peer = function(){}

Peer.prototype.save = function(data){
    const peer = new PeerSchema(data)
    peer.save()
}

Peer.prototype.flush = function(data){
    PeerSchema.deleteMany(data, function(err, doc){
        console.log("DELETED: ", doc)
    })
}

Peer.prototype.getJoins = function(data, cb){
    PeerSchema.findOne(data, function(error, doc){
        const exist = (!error && doc != null)
        cb(exist)
    })
}

module.exports = new Peer()