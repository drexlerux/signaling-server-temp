const mongoose = require('mongoose')
const Schema = mongoose.Schema
const moment = require('moment-timezone')

var InterviewSchema = new Schema({
    room:  {
        type: String,
        index: true,
        unique: true
    },
    status: {
        type: String,
        enum: ['STARTED', 'DONE', 'MEDIA_PROCESSED']
    },
    idInterviewer: Number, 
    interviewer: String,
    interviewed: String,
    recordTimingLeft: {
        timeLength: Number,
        distance: Number,
        minutes: Number,
        seconds: Number
    },
    //userName: [String],
    date: { type: Date, default: Date.now},
})

InterviewSchema.virtual('dateFrt').get(function () {
    return moment(this.date).tz('America/Bogota').format('YYYY-MM-DD hh:mm a')
});

module.exports = mongoose.model('interviews', InterviewSchema)