const mongoose = require('mongoose')
const Schema = mongoose.Schema
var ChatSchema = new Schema({
    clientId:  String,
    role: String,
    room: {type: String, ref: 'interviews'},
    date: { type: Date, default: Date.now},
})

module.exports = mongoose.model('peers', ChatSchema)