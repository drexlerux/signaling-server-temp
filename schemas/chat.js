const mongoose = require('mongoose')
const moment = require('moment-timezone')

const Schema = mongoose.Schema

var ChatSchema = new Schema({
    content:  String,
    category: String,
    room: String,
    role: String,
    username: String,
    createdAt: { type: Date, default: Date.now},
},
{
    toJSON: { virtuals: true },
}
)


ChatSchema.virtual('date').get(function () {
    console.log("mierda ")
    return moment(this.createdAt).tz('America/Bogota').format('YYYY-MM-DD')
});

ChatSchema.virtual('time').get(function () {
    return moment(this.createdAt).tz('America/Bogota').format('hh:mm a')
});


module.exports = mongoose.model('chats', ChatSchema)