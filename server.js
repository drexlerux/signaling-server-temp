var express = require("express")
var http = require("http") 
var path = require("path") 
var bodyParser = require('body-parser')
var logger = require('morgan')
var signaling = require('./signaling')

var indexRoutes = require('./routes')

var mongoose = require('mongoose')

var flash = require('express-flash')

var errors = require('./middlewares/errors') 

var session = require('express-session')

var cookieParser = require('cookie-parser');

var passport = require('passport')

var LocalStrategy = require('passport-local').Strategy


global.log = console.log

global.chalk = require('chalk')

global.ssn = {isAuth: false};

global.env = process.env.NODE_ENV.trim() || 'dev';


const publicDirs = [
  'public',
  'node_modules/vue/dist',
  'node_modules/socket.io-client/dist',
  'node_modules/font-awesome',
  'node_modules/jquery/dist',
  'node_modules/bootstrap/dist',
  'node_modules/sweetalert2/dist',
  'node_modules/toastr/build',
  'node_modules/clipboard/dist'
]


var app = express()

app.use(logger('dev'))

app.use(express.json())

app.use(bodyParser.json()); // for parsing application/json

app.use(bodyParser.urlencoded({ extended: false })); // for parsing application/x-www-form-urlencoded



app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, secret  ")
  next()
})

app.set('views', path.join(__dirname, 'views'))

app.set('view engine', 'pug');

app.use(cookieParser())
app.use(session({  
  /*genid: function(req) {
    return genuuid() // use UUIDs for session IDs
  },*/
  secret: 'lkklklklkl',
  resave: true,
  saveUninitialized: true
}))

app.use(passport.initialize());
app.use(passport.session());
app.use(flash())

passport.serializeUser(function(user, done) {
  //console.log(user.name);
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

publicDirs.forEach(function(dir){
  app.use(express.static(path.join(__dirname, dir)))
})



app.use('/', indexRoutes);

app.use(errors._404);
//app.use(errors.handler);

passport.use('local', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: true
},require('./utils/auth')));


app.locals.range = function(start, end) {
  var arr = [];
  for (var i = start; i < end; i++) arr.push(i);
  return arr;
}

var server = http.createServer(app)

/**
 * Connect to mongo
*/

mongoose.connect('mongodb://localhost:27017/chat', { useNewUrlParser: true }).then(function(db){ 
  	log(chalk.blue('db connected on '+ env)) 
  	server.listen(3000)
  	server.on('error', onError)
	server.on('listening', onListening)
	signaling(server)	
}).catch(function(err){ console.log(err) })

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges')
      process.exit(1)
      break
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
      break
    default:
      throw error
  }
}


/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
	var addr = server.address()
	var bind = typeof addr === 'string'
	? 'pipe ' + addr
	: 'port ' + addr.port
	log(chalk.green('Listening on ' + bind))
}
