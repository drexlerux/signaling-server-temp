module.exports = function(req, res, next) {
	if (req.isAuthenticated())
    	return next();
  			// IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
  	res.redirect('/');
}
