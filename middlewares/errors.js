module.exports = {
	_404: function(req, res, next) {
		res.status(400);
		res.render('errors/404');
	},


	_500: function(req, res, next) {
		res.status(400);
		res.render('errors/500');
	}
}